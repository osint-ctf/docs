# Olympics MAVERIS CTF
https://olympics.maveris.com/challenges

## Mile 1
### Text
I am the Board Chair of the U.S. Olympic & Paralympic Committee but I was not always in that role. Back in the early 2000’s I worked at a company for a year. If you were working for the media and wanted to contact that company to find out about my time there, you would use this email for media inquiries?

### Flag
On the U.S. Olympic & Paralympic Committee website, we can see its members. Suzanne Lyons is the Board Chair ; we can check her Linkedin prodile, we see that she wordked for Russell Reynolds Associates for a year. Next we cab go to this site : https://www.signalhire.com/companies/russell-reynolds-associates where we can retrieve the flag : 

`flag : emily.heinen@russellreynolds.com`

## Mile 2
### Text
This sporting event was first introduced at the 1996 Olympics. For this year's Olympics there was a lot of excitetment about the course. There was a short video posted on social media in October of 2019 showing some athletes practicing on the actual course. The video shows the group jumping with Mount Fuji in the background.

What is the word that can be read on the front of the uniform of the athlete while they are in mid-air?

The answer is just one word.

### Flag
On the Wikipedia page for the '96 Olympics Games, we can see the sports introduced in the event : Softball, beach volleyball and mountain biking.

'course' may be refrring to mountain biking. We can google that and choose the time period :

![](images/2022-01-24_16-29.png)

https://www.pinkbike.com/news/social-roundup-first-look-at-the-olympic-mtb-course-tokyo-2020-test-event.html

![](images/2022-01-24_16-28.png)

`flag : france`

## Mile 3
### Text
This person looks to be getting ready to race at this Olympic site.

![](images/challenge14.jpeg)

People need to be careful when they share vacation photos online, some platforms don’t always strip metadata from the images. Where was this picture taken; name the "site" (use full site name seen on Google Maps, the flag is not a URL)?
### Flag
We can look at the EXIF dara of the image and locate the GPS coordinates on google :
![](images/2022-01-24_17-05.png)

`flag : Stadium of the Asclepieion of Epidaurus`

## Mile 4
### Text
This 2019 World Cup championship team failed to qualify for the olympics this year even though they are still considered one of the top teams in the world. This inaugural sport is won by scoring how many points?

### Flag

These sports were added to the 2020 Olympics Games : baseball/softball, karate, sport climbing, surfing, and skateboarding.

There was no 2019 championship for karate (https://en.wikipedia.org/wiki/Karate_World_Championships#List_of_Karate_World_Championships).

No championship either for baseball.

Skateboarding : https://olympics.com/en/news/everything-about-2019-world-skate-park-championships

Sport climbing : https://en.wikipedia.org/wiki/2019_IFSC_Climbing_World_Championships

Surfing : https://en.wikipedia.org/wiki/2019_ISA_World_Surfing_Games

WIP

## Mile 5
### Text
You may be surprised at the different events that have occurred at the Olympics over the ages. There was an actual event that was more artistic in nature and an American won an actual medal for a design. The best part was that the design then was never turned into reality.

Find an image of a copy (or if you can find the original) of the drawing for the design plans and then add up how many playgrounds are depicted on it.

### Flag
https://en.wikipedia.org/wiki/Art_competitions_at_the_Summer_Olympics

These art were competition for the Olympic Games in its early days : architecture, literature, music, painting, and sculpture. "playgrounds" may be referring to architecture.

USA won a medal in architecture at the 1936 Olympic Games : Charles Downling Lay for its drawing of Marine Prk in Brookli.

https://mapcollections.brooklynhistory.org/map/marine-park-brooklyn-new-york-city-copied-from-drawing-of-charles-downing-lay-landscape-architect-101-park-ave-new-york-city/

![](images/bhs_ba004031441_a-scaled.jpg)

We can see 3 mentions of "playground".

![](images/2022-01-24_17-45.png)

`flag : 3`

## Mile 6
### Text
For the romantics out there:
A certain Scandinavian prince met his future bride while attending the summer Olympics in her home country. Locate the name of the local watering hole where they met and tell me what country's flag has flown at the front of the establishment for at least the last couple of years.

## Mile 7
### Text
This military officer competed in the Modern Pentathlon in the Olympics for the US back when gold medals were still solid gold. On his gravestone, what is the second line?
### Flag
The gold medals of these Olympic Games were made of gold (https://olympics.com/en/olympic-games/olympic-medals) :

- Antwerp 1920
- Stockholm 1912
- London 1908
- St. Louis 1904

According to https://en.wikipedia.org/wiki/Modern_pentathlon_at_the_Summer_Olympics, this sport was first introduced in 1912.

https://en.wikipedia.org/wiki/Modern_pentathlon_at_the_1912_Summer_Olympics : General Patton is the military officer searched.

https://commons.wikimedia.org/wiki/File:General_Patton's_grave_300806.jpg

![](images/General_Patton's_grave_300806.jpg)

`flag : general third army`

## Mile 8
### Text
The Olympics always have a great torch relay.

Find where the torch was before it went to Okinawa on its journey. It was at this location for 2 days.

Can you get the details from the Day-1 book? The answer is just the file name of the pdf you can download not the full url (filename.pdf submit filename without the pdf part).

# CTF from The SEINT

The CTF is on [The SEINT's Gitlab](https://github.com/seintpl/osintquiz/tree/main/2022)

## step 0

MD5 checksum of the word "summer" : `6b1628b016dff46e6fa35684be6acc96`, wich is the password for the zip file.

## step 1
`
I came to a city that welcomed me with a puzzle. While sightseeing I was tricked to go to the train station an hour earlier. And that's all because of the cathedral bells ringing noon not at the time you'd expect them to. While I ran down from the hill along a little street named after a president, just behind that cathedral I noticed a skull painted by a gate. Just behing that gate somebody left a piece of cloth. Its color is the password that will let you find my next exact location.
`

When Googling a "cathedral that rings its bells at 11 AM instead of noon", we got a link to an article about the cathedral of Saint Peter and Saint Paul in Brno, Czech Republic. This cathedral is located on Petrov Hill. The street around the cathedral is also named Petrov, according to Google maps. Petrov is the name of the russian president in the TV series, *House of Cards*.

By looking around with Google street view, we can see the skull described in the text :
![](images/step1_skull.png)
and if we look behind, we see this piece of purple fabric :
![](images/step1_cloth.png)

The flag for step2 is `bb7aedfa61007447dd6efaf9f37641e3`

## step 2
`
Going to 727774447777
`

This code can be T9 encoded text, If we look at a keypad, we can decipher this to Paris.
![](images/step2_keypad.png)

The flag for step3 is `ccbee73cd81c7f42405e1920409247ec`

## step 3
`
I know you found the cloth in Brno. The colour reminded me of a song and a movie from 1984 that had the same title. I remembered there was a place named just like the song and wanted to find it while being in Paris. I found the place on the map, but the shop wasn't there. People said it moved some time ago. I came to the nearest intersection and tried to look left first, but the sun was shining right into my eyes, just along the street. What was the time?
`

The song and the movie the message is referring to is *Purple Rain*. There was a shop in Paris named like this as seen in this Google street view from 2018 :
![](images/step3_shop.png)
We can go to www.suncalc.org to find the exact time as described in the message :
![](images/step3_suncalc.png)

The flag for step4 is `a3c2c70e2d306f1ea5f270337eeb31cb`

## step 4
`
I know you like puzzles, so I'm going to give you a clue about my next location, but first I have to tell you where I am right now. I moved to a country where I can really get The Sun. I am standing on a bridge between two towers and have a glass floor below me. It's incredible that I can actually see a ship sailing below me. I think someone said they're going to post it on Twitter today. The name of the place I'm travelling to next is where the ship is from.
`

The bridge described in the message could be The tower bridge in London. We can use this Twitter search string to find the next location : `from:@TowerBridge until:2022-08-08 since:2022-08-06`

![](images/step4_towerbridge.png)

The next location is *Sweden*

The flag for step5 is `88e52ee42fd090896f69d0d71e339d94`

## step 5
`
I'm both in Sweden and in the USA right now. How is it possible? Well, there's a town with that name in the north of the US. It has a nice website, but fifteen years ago this town and website looked a little different. On the main page there was a picture of a building I am standing in front of right now. There's someone in a car parked right here, talking on the phone and another car with some strange gear just passed by. Can you tell the parked car's make and model?
`

Probably the hardest step so far. I felt down the rabbit hole by looking US cities that share their names with Swedish cities.

I found [this site](https://allmogens.se/en/20-svensk-amerikanska-ortnamn/) which lists cities with that description but none of them had a site with a  picture building, back in 2007.

In fact the message give the answer if we read it carefully. `I'm ... in Sweden ... there's a town with that name in the north of the US.` The town we are looking for is called Sweden, and is located in the state of New York.

The website actually shows of building in 2007.
![](images/step5_sweden.png)

If we look at Google Street view, we can see a car parked with someone on the phone.
![](images/step5_car1_side.png)
![](images/step5_car1_front.png)
![](images/step5_car1_rear.png)

This car is an Infiniti model G35. I couldn't see the exact model number but by doing a google image search, I found it.
![](images/step5_google_image.png)

The flag for step 6 is `2d94892c7f81e83990d63e8e59fd1a5b`

## step 6
`
This trip is over. I'm glad you accomapnied me virtually. Can you figure out just one more thing? Last year I found a nice cafe. It was just beside one of the places I visited last year. I will give you a clue of where I was - look at the attached files. The last one is from there. I remember I had to head north for about 10 minutes, under some green bridges and by a sign telling that the maximum height beneath that bridge is 4.2 m. The name of the cafe was something something cafe and one more word after that. Will you be able to find it?
`

[Website used](https://www.nmeagen.org/)

File #1 : Beijing, China

![](images/step6_file1.png)

File #2 : London

![](images/step6_file2.png)

File #3 : Rio de Janeiro, Brazil

![](images/step6_file3.png)

Those cities hosted Summer Olympic Games in, respectively, 2008, 2012 and 2016. If we follow this pattern for file #4, which is corrupted, it would be Tokyo. More precisely, the GPS data files show the Olympic stadiums where the opening ceremony took place.

So if we search for a "cafe" at the north of the Japan Olympic Stadium, we can find the "Good Morning Cafe Nowadays".

![](images/step6_map.png)

Near a green bridge.

![](images/step6_bridge.png)

![](images/step6_cafe.png)

The flag is `2b849500e4585dab4196ec9a415edf8f`
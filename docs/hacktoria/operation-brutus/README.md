# operation-brutus

https://hacktoria.com/monthly-ctf/operation-brutus

# shipping-handler-instructions
## original

## translation
### in english
Hi, Here's a quick note on hunting and shipping this month. The usual hunting grounds and animals, this time may bring a Sheng language interpreter, call +254 41 555 555 0 if required. Once you have 200 animals, schedule the shipment and make sure you have the tracking details ready for the customer on the website. Your marketing contact is Wendy Liu who will bring her ID and wait at the dock. At some point, she may email you information about the shipment ID. Don't worry she can speak Chinese, not English this time. She has local transportation arrangements to transport the goods to the market. She will be wearing a blue raincoat and red scarf for identification.
### in french
Salut, Voici une note rapide sur la chasse et l'expédition ce mois-ci. Les terrains de chasse et les animaux habituels, cette fois peuvent amener un interprète en langue Sheng, appelez le +254 41 555 555 0 si besoin. Une fois que vous avez 200 animaux, planifiez l'expédition et assurez-vous que les détails de suivi sont prêts pour le client sur le site Web. Votre contact marketing est Wendy Liu qui apportera sa carte d'identité et attendra sur le quai. À un moment donné, elle peut vous envoyer par e-mail des informations sur l'ID de l'envoi. Ne vous inquiétez pas, elle peut parler chinois, pas anglais cette fois. Elle a des arrangements de transport locaux pour transporter les marchandises au marché. Elle portera un imperméable bleu et une écharpe rouge pour identification.
# The Mona Lisa Heist

## Target Paintings

Watercolors – Marc Chagall

An Allegory – Sandro Botticelli

La Bella Principessa – Leonardo da Vinci

Portrait of Alexander Mornauer – Hans Holbein

The passion – Hans Holbein

Lady with an ermine – Leonardo da Vinci

The Annunciation – Sandro Botticelli

America Windows – Marc Chagall

The School of Athens – Raffaello Sanzio da Urbino

The Starry Night – Vincent van Gogh

American Gothic – Grant Wood

The Persistence of Memory – Salvador Dali

Mona Lisa – Leonardo da Vinci

## Mysterious email

```
Alright one of the paintings we're going to hit has it's description hidden in the riddle that follows from the resolution below:

0-4      PqBnrdaO75Y
0-2      CdZDXKVuMlE
0-2      D7vCxL45Jn8
2-5      1GEFhh1kxuY
0        Yykfw9eNA5s
1-4      v72zb9_dxnA
2-3      Eo4s1o-u1wU
2-5      XAH4Ovuyjxg
0        l0WKYlaqKq4
1-3      9T6HGC-m8zU
1        ipf7ifVSeDU
1        1EpuHGMn7B0
0-2      dS71chsx104

Other heist is included in the plans with images of how we're doing that one. You will receive the details on the exact locations in another way.

/;;7:_TT)0;S3@TXt;U.\w
```

https://www.youtube.com/watch?v=PqBnrdaO75Y : "I am the god of war"

https://www.youtube.com/watch?v=CdZDXKVuMlE : "What is relative truth and absolute truth?"

https://www.youtube.com/watch?v=D7vCxL45Jn8 : "Moving in circles"

https://www.youtube.com/watch?v=1GEFhh1kxuY : "Cinematic Journey Through the Solar System"

https://www.youtube.com/watch?v=Yykfw9eNA5s : "Captured"

https://www.youtube.com/watch?v=v72zb9_dxnA : "Walking at an Obscure Time - a song for band."

https://www.youtube.com/watch?v=Eo4s1o-u1wU : "Amelie Ziino accompanied by Luca Ziino"

https://www.youtube.com/watch?v=XAH4Ovuyjxg : ""THIS IS MY SON, WHOM I LOVE", Mk 9:1~13, 04/30/2022 / UBF Daily Bread"

https://www.youtube.com/watch?v=l0WKYlaqKq4 : "DESPISE - "Wounds" (Official Audio)"

https://www.youtube.com/watch?v=9T6HGC-m8zU : "Energy Stored in a Capacitor"

https://www.youtube.com/watch?v=ipf7ifVSeDU : "Relaxing Meadow with Ambient Nature Sounds, Wildflowers, and Mountain View - 8 Hours"

https://www.youtube.com/watch?v=1EpuHGMn7B0 : "Days of our Lives: Beyond Salem | Chapter 2 | Official Trailer | Peacock Original"

https://www.youtube.com/watch?v=dS71chsx104 : "A King's Land"

## Heist plans

Kunsthistorisches Museum Wien

## Real or fake ?

Painting 5 is a fake by David Steain, in tribute to Marc Chagall

## next heist
Christuskind mit Johannesknaben und zwei Engeln
# Follow the black rabbit

[https://hacktoria.com/collaborations/follow-the-black-rabbit/](https://hacktoria.com/collaborations/follow-the-black-rabbit/)

## vault

`unknown-backup/home/UNTITLED/black-bunny-organization-card.jpeg`

![](images/black-bunny-organization-card.jpeg)

We send an email to `blackbunnyhodeout@gmail.com` with the secret found in the text file in the same folder `blackrabbits4life-operation-coffee`.

We get a link to the `beans.zip` file as an answer and this text :

```

G00d afternoon,

to retrieve the coffee-beans you have deposited, please proof that you are ****** by answering the following question: 
```
![](images/email.png)
```
The-coordinates is the password for the beans in this vault: https://drive.google.com/file/d/158EIYAW1IJvYfNkFxUA92yeBGVN_jPbF/view?usp=sharing
```

## Geolocation

![](images/password.png)

Cologne Cathedral, Germany

## unknown-backup/etc/P455W0RD

```
name-of-structure-contry-city

use password for vault
```
I tried to unzip beans.zip and vault.zip

✖️ cathedral-cologne-germany

✖️ cathedral-hurch-of-saint-peter-cologne-germany

kölner-dom-cologne-germany

## Decoding the files

`unknown-backup/root/password.txt` : binary data, decoded to "Never gonnar give you up" lyrics

`unknow-backup/etc/PASSWORD` : base-64 data, "Never gonna give you up" lyrics

### corrupt-coffe-recipe

```
Recipe for the "=%§!S&/%" coffee


Material:

1. 5 gr4m5 of 3UE$%jkj/H§JLF=R c0ff33 b3an5
2. 1 TP 0f Br0wn 5u94r
3. 1 B4r of %"§)/" ch0colate

Brewing the c0ff3

First step: 籠籮鰊类籮 籷籸 籼籽类籪籷籰籮类籼 籽籸 籵籸籿籮 籢籸籾 籴籷籸粀 籽籱籮 类籾籵籮籼 籪籷籭 籼籸 籭籸 籒

Second step: p 7F== 4@>>:E>6?EVD H92E x′> E9:?<:?8 @7 *@F H@F=5?VE 86E E9:D 7C@> 2?J @E96C 8FJ

Third step: ayA7U1FSIFUyPz8yIFI2PT0gV0BTIDlAVSBrST4gNzY2PTo/OAppQFJSMiA+Mjw2IFdAUyBTPzU2UFFSMj81Cgp9NlQ2UCA4QD8/MiA4OlQ2IFdAUyBTTgp9NlQ2UCA4QD8/MiA9NlIgV0BTIDVAVT8KfTZUNlAgOEA/PzIgUFM/IDJQQFM/NSAyPzUgNTZRNlBSIFdAUwp9NlQ2UCA4QD8/MiA+Mjw2IFdAUyA0UFcKfTZUNlAgOEA/PzIgUTJXIDhAQDUzVzYKfTZUNlAgOEA/PzIgUjY9PSAyID06NiAyPzUgOVNQUiBXQFM=

Last step: 9ccb9a7213f7a3966d852dc4e23cd4999c0270912a0c92a790a718c257912c21e8051947950b6ded4d1a2c8cca7c08b03cb9435b0e52717ad4e4f523c8f135d8
```

`First step` translates to

`The residue of the new baskets is the residue of the flange and the flange is the residue of the residue and the residue of the germs and the germs. The best way to redeem an indica residue`

`Second step` : ROT47

`A full commitment's what I′m thinking of You wouldn't get this from any other guy`

`Third step` : Base-64 and ROT47 encoded text, decoded as

```
< j$"# &anna #ell (o$ ho& <xm feeling
:o##a make (o$ $nde!"#and

Ne%e! gonna gi%e (o$ $}
Ne%e! gonna le# (o$ do&n
Ne%e! gonna !$n a!o$nd and de"e!# (o$
Ne%e! gonna make (o$ c!(
Ne%e! gonna "a( goodb(e
Ne%e! gonna #ell a lie and h$!# (o$
```

`Last step` seems to be a SHA512 hash

### character substitution

| ! | % | $ | ( | } | " | # | & | < | : |
|---|---|---|---|---|---|---|---|---|---|
| r | v | u | y | p | s | t | w | I | G |

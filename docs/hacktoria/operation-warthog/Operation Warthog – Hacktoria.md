**Operation Warthog**

**from: dimzech555@tiberianserpent.com**  
**to: tmp-agnt007@tiberianserpent.com**  
**date: Jan 01, 2022, 9:05 AM**  
**subject: Briefing Operation Warthog**

Hi and welcome to the team!

I’ve heard a lot about you and your achievements, very honored to work with you. As Julia may have filled you in, I’m Dimitri, head of the Ream Team Operations here at Tiberian Serpent. Since we don’t have too much time for onboarding you, I’ll give you a quick overview of how we work. Given your experience, you won’t need too much hand holding anyway.

-   Team Leads are responsible for making decisions, we don’t do extra management layers. Reach out to your team members on Twitter if you need help.
-   We do not really exist, our mandate stretches far beyond what a normal operative is allowed to do. But only regarding your target of course. 
-   We pick our own tools, the only standard we have is where we report our findings and in what format.

Good, on with the task at hand. Last week during the Christmas holidays, we were able to compromise a smart fridge in the home of Maksim Kotova, a former politician during the Cold War in the USSR. This fridge exposed the WiFi password for his home router. Allowing Julia to go near his home and find more information. She was needed on site to verify it’s really him, take pictures of the surrounding area and get access to his devices.

Using an Evil Access Point, we got his phone to connect and were able to do a packet capture. After finding the IP addresses associated with the Mail RU service, we crafted a landing page to capture his email credentials. The account we got access to was fairly scrubbed clean, but the last message was still in there. It is now clear that Maksim Kotova is part of the Order of Hades. His exact role, we don’t know yet, that’s where you come in.

Since he seems to be responsible for arranging shipments, we need your team to find as much information as possible on Maksim Kotova. Once we know who he is, find out where and when the shipment will take place.

**Objective:** Report the _location of the shipment_, _time_ and your _callsign_ in a Direct Message to the [Tiberian Serpent Twitter](https://twitter.com/tiberianserpent) account.

**Format:** Operation Warthog, agent "CALLSIGN" reporting "LOCATION" at "DATE - TIME".
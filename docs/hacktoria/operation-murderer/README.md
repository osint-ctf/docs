# Operation MurderER


## Step 2

Three images to geo-locate :

![](location-01.jpg)

Place : Loretto Hospital

Street : Crossing of West Flournoy Street and South Central Avenue

GPS : 41.871943936285845, -87.76445419155206

City : Chicago

![](location-02.jpg)

Place : Mount Emblem Cemetery

GPS : 41.92917094527163, -87.92334109714072

City : Elmhurst

![](location-03.jpg)

Place : Holmgren Athletic Complex

Street : 3055 West Forster Name

GPS : 41.97542980345607, -87.70515891440965

City : Chicago

## Step 3

We got a link to a Youtube video : https://www.youtube.com/channel/UCyOofwsh3r5FlC3tr1mJQoA

My CTF techniques immediately told me the sound needs to be viewed in a frequency analyzer, so we can use Audacity for this purpose

![](stereogramme.png)

https://theyarehereamongus.blogspot.com/

## Step 4

We have this message on the previous website :

![](blogpost.png)

It may be hard to see but there are some letters in italic. I selected the text, right clicked and clicked on view the source code off the selection (I don't have the actual name in the english interface). Copy/paste in a text editor, and now it's a matter of searchind the `<i>` and `</i>` markups.

`fillsballotsnecks`

seems like an anagram

into lake flsblltscs
in fllsballotsecks

softball illsnecks

## Step 5

https://twitter.com/MarleneObdam



mrwhiskers1260@gmail.com

GG-WP-MUCHOSINT

## Step 6

Password : MisterBootsCaptainFluffyMrsMittens

Places to look :

111 West Laramie Drive, Reno, NV, USA